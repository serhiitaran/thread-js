export default [
  '/auth/login',
  '/auth/register',
  '/auth/password/recover',
  '/auth/password/reset'
];
