import env from '../env';

export const { sendgridApiKey, fromEmailAdress } = env.mailService;
