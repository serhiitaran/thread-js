import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const updateCommentById = (id, data) => commentRepository.updateCommentById(id, data);

export const deleteCommentById = id => commentRepository.deleteById(id);

export const setReaction = async (userId, { commentId, isLike = true }) => {
  const updateOrDelete = react => (react.isLike === isLike
    ? commentReactionRepository.deleteById(react.id)
    : commentReactionRepository.updateById(react.id, { isLike }));

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  return Number.isInteger(result) ? {} : commentReactionRepository.getCommentReaction(userId, commentId);
};

export const getCommentReactionUsers = ({ commentId, isLike }) => {
  const commentReactions = commentReactionRepository.getCommentReactions(commentId, isLike);
  return commentReactions.map(react => react.user);
};
