import { sendgridApiKey, fromEmailAdress } from '../../config/mailConfig';
import { passwordResetMsg, likePostNotificationMsg, sharedPostMsg } from '../../helpers/mailMessages';

const sgMail = require('@sendgrid/mail');

export const sendPasswordResetEmail = async (userEmail, resetPasswordToken) => {
  sgMail.setApiKey(sendgridApiKey);
  const linkToResetPassword = `http://localhost:3000/password/reset/${resetPasswordToken}`;
  const msg = passwordResetMsg(userEmail, fromEmailAdress, linkToResetPassword);
  try {
    await sgMail.send(msg);
  } catch (error) {
    throw new Error('Unable to send email');
  }
};

export const sendLikePostNotification = async (userEmail, userWhoLiked, postId) => {
  sgMail.setApiKey(sendgridApiKey);
  const postLink = `http://localhost:3000/share/${postId}`;
  const msg = likePostNotificationMsg(userEmail, fromEmailAdress, userWhoLiked, postLink);
  try {
    await sgMail.send(msg);
  } catch (error) {
    throw new Error('Unable to send email');
  }
};

export const sendSharedPost = async (emailToShare, postId, userWhoShared) => {
  sgMail.setApiKey(sendgridApiKey);
  const postLink = `http://localhost:3000/share/${postId}`;
  const msg = sharedPostMsg(emailToShare, fromEmailAdress, userWhoShared, postLink);
  try {
    await sgMail.send(msg);
  } catch (error) {
    throw new Error('Unable to send email');
  }
};
