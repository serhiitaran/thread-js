import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import { getUserById } from './userService';
import { sendLikePostNotification, sendSharedPost } from './mailService';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const updatePostById = (id, data) => postRepository.updatePostById(id, data);

export const deletePostById = id => postRepository.deleteById(id);

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const getPostReactionUsers = ({ postId, isLike }) => {
  const postReactions = postReactionRepository.getPostReactions(postId, isLike);
  return postReactions.map(react => react.user);
};

export const notifyPostLiked = async (postUserId, postId, { username: userWhoLiked }) => {
  const { email: postUserEmail } = await getUserById(postUserId);
  sendLikePostNotification(postUserEmail, userWhoLiked, postId);
};

export const sharePost = async (emailToShare, postId, { username: userWhoShared }) => {
  sendSharedPost(emailToShare, postId, userWhoShared);
  return { success: true };
};
