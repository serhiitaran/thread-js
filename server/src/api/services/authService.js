import { createToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import userRepository from '../../data/repositories/userRepository';
import { sendPasswordResetEmail } from './mailService';

const crypto = require('crypto');

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password)
  });
  return login(newUser);
};

export const recoverPassword = async email => {
  if (!email) {
    throw new Error('Email is required!');
  }

  const user = await userRepository.getByEmail(email);
  if (!user) {
    throw new Error('User with this email was not found!');
  }
  const token = crypto.randomBytes(20).toString('hex');
  const tokenExpires = Date.now() + 3600000;
  const resetPasswordToken = await userRepository.setResetPasswordToken(user.id, token, tokenExpires);
  await sendPasswordResetEmail(email, resetPasswordToken);
  return { success: true };
};

export const checkResetPasswordToken = async resetPasswordToken => {
  const user = await userRepository.getUserByResetPasswordToken(resetPasswordToken);

  if (!user || !user.resetPasswordExpires > Date.now()) {
    throw new Error('Token is not valid');
  }

  return { success: true };
};

export const changePassword = async (resetPasswordToken, password) => {
  const user = await userRepository.getUserByResetPasswordToken(resetPasswordToken);

  await userRepository.updateUserById(user.id, {
    password: await encrypt(password),
    resetPasswordToken: null,
    resetPasswordExpires: null
  });

  return { success: true };
};
