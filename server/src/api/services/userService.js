import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, status, email, imageId, image } = await userRepository.getUserById(userId);
  return { id, username, status, email, imageId, image };
};

export const updateUserById = (userId, data) => userRepository.updateUserById(userId, data);
