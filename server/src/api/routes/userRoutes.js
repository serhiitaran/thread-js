import { Router } from 'express';
import * as userService from '../services/userService';

const router = Router();

router
  .put('/:id', (req, res, next) => userService.updateUserById(req.params.id, req.body)
    .then(user => res.send(user))
    .catch(next));

export default router;
