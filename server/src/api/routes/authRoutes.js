import { Router } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/register', registrationMiddleware, (req, res, next) => authService.register(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/password/recover', (req, res, next) => authService.recoverPassword(req.body.email)
    .then(data => res.send(data))
    .catch(next))
  .get('/password/reset', (req, res, next) => authService.checkResetPasswordToken(req.query.token)
    .then(data => res.send(data))
    .catch(next))
  .post('/password/reset', (req, res, next) => (
    authService.changePassword(req.query.token, req.body.password)
      .then(data => res.send(data))
      .catch(next)))
  .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById(req.user.id)
    .then(data => res.send(data))
    .catch(next));

export default router;
