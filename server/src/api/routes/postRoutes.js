import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/react', (req, res, next) => postService.getPostReactionUsers(req.query)
    .then(users => res.send(users))
    .catch(next))
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        const isLiked = req.body.isLike;
        if (isLiked) {
          postService.notifyPostLiked(reaction.post.userId, req.body.postId, req.user);
          req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
        } else {
          req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked!');
        }
      }
      req.io.emit('react_post', { userId: req.user.id, postId: req.body.postId });
      return res.send(reaction);
    })
    .catch(next))
  .post('/share', (req, res, next) => postService.sharePost(req.body.email, req.body.postId, req.user)
    .then(data => res.send(data))
    .catch(next))
  .put('/:id', (req, res, next) => postService.updatePostById(req.params.id, req.body)
    .then(post => {
      req.io.emit('uptated_post', post);
      return res.send(post);
    })
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deletePostById(req.params.id)
    .then(post => {
      req.io.emit('deleted_post', post);
      return res.send(post);
    })
    .catch(next));

export default router;
