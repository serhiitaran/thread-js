import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/react', (req, res, next) => commentService.getCommentReactionUsers(req.query)
    .then(users => res.send(users))
    .catch(next))
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => {
      req.io.emit('new_comment', comment);
      return res.send(comment);
    })
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.comment && (reaction.comment.userId !== req.user.id)) {
        const isLiked = req.body.isLike;
        if (isLiked) {
          req.io.to(reaction.comment.userId).emit('like_сomment', 'Your comment was liked!');
        } else {
          req.io.to(reaction.comment.userId).emit('dislike_comment', 'Your comment was disliked!');
        }
      }
      req.io.emit('react_comment', { userId: req.user.id, commentId: req.body.commentId });
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => commentService.updateCommentById(req.params.id, req.body)
    .then(comment => {
      req.io.emit('updated_comment', comment);
      return res.send(comment);
    })
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteCommentById(req.params.id)
    .then(comment => {
      req.io.emit('deleted_comment', comment);
      return res.send(comment);
    })
    .catch(next));

export default router;
