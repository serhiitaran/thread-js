export const passwordResetMsg = (userEmail, fromEmailAdress, linkToResetPassword) => {
  const msg = {
    to: userEmail,
    from: fromEmailAdress,
    subject: 'Thread-js Password Reset',
    text: `
      We got a request to reset your Thread-js password.
      Please click on the following link: ${linkToResetPassword}
      If you ignore this message, your password will not be changed.`
  };
  return msg;
};

export const likePostNotificationMsg = (userEmail, fromEmailAdress, userWhoLiked, postLink) => {
  const msg = {
    to: userEmail,
    from: fromEmailAdress,
    subject: 'Thread-js Notification',
    text: `
      Hello!
      Your post was liked by ${userWhoLiked}.
      Post: ${postLink}`
  };
  return msg;
};

export const sharedPostMsg = (emailToShare, fromEmailAdress, userWhoShared, postLink) => {
  const msg = {
    to: emailToShare,
    from: fromEmailAdress,
    subject: 'Thread-js Notification',
    text: `
      Hello!
      ${userWhoShared} shared a post with you.
      Have a look: ${postLink}`
  };
  return msg;
};
