export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('users', 'resetPasswordToken', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('users', 'resetPasswordExpires', {
        type: Sequelize.DATE
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'resetPasswordToken', { transaction }),
      queryInterface.removeColumn('users', 'resetPasswordExpires', { transaction })
    ]))
};
