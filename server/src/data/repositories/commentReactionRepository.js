import { CommentReactionModel, CommentModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: { userId, commentId },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getCommentReactions(commentId, isLike) {
    return this.model.findAll({
      group: [
        'commentReaction.id',
        'user.id',
        'user->image.id'
      ],
      where: {
        commentId,
        isLike
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }]
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
