import { UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class UserRepository extends BaseRepository {
  addUser(user) {
    return this.create(user);
  }

  getByEmail(email) {
    return this.model.findOne({ where: { email } });
  }

  getByUsername(username) {
    return this.model.findOne({ where: { username } });
  }

  getUserById(id) {
    return this.model.findOne({
      group: [
        'user.id',
        'image.id'
      ],
      where: { id },
      include: {
        model: ImageModel,
        attributes: ['id', 'link']
      }
    });
  }

  async updateUserById(userId, data) {
    const { username } = data;
    if (username) {
      const user = await this.getByUsername(username);
      const userExits = user && user.id !== userId;

      if (userExits) {
        throw new Error('This username is already taken. Please choose another one.');
      }
    }

    await this.updateById(userId, data);
    return this.getUserById(userId);
  }

  getUserByResetPasswordToken(resetPasswordToken) {
    return this.model.findOne({ where: { resetPasswordToken } });
  }

  async setResetPasswordToken(userId, resetPasswordToken, resetPasswordExpires) {
    await this.updateById(userId, { resetPasswordToken, resetPasswordExpires });
    return resetPasswordToken;
  }
}

export default new UserRepository(UserModel);
