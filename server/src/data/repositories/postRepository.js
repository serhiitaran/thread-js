import { Op } from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

const commentLikeCase = bool => `
  SUM(CASE WHEN "comments->commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END)
  OVER(PARTITION BY "comments"."id")`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      filterType,
      userId
    } = filter;

    const where = {};
    if (userId) {
      let postsFilter = {};
      switch (filterType) {
        case 'showOwnPosts':
          postsFilter = { userId };
          break;
        case 'hideOwnPosts':
          postsFilter = { userId: { [Op.ne]: userId } };
          break;
        case 'showLikedByMePosts':
          postsFilter = {
            id: {
              [Op.in]: [
                sequelize.literal(`
                  (SELECT "postId" FROM "postReactions"
                  WHERE "postReactions"."userId" = '${userId}'
                  AND "postReactions"."isLike" = true)
                `)
              ]
            }
          };
          break;
        default:
          break;
      }
      Object.assign(where, postsFilter);
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'comments->commentReactions.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        attributes: {
          include: [
            [sequelize.literal(commentLikeCase(true)), 'likeCount'],
            [sequelize.literal(commentLikeCase(false)), 'dislikeCount']
          ]
        },
        include: [{
          model: UserModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: CommentReactionModel,
          attributes: []
        }
        ]
      }, {
        model: UserModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }

  async updatePostById(id, data) {
    await this.updateById(id, data);
    return this.getPostById(id);
  }
}

export default new PostRepository(PostModel);
