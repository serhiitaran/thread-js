import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { checkResetPasswordToken, changePassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import ResetPassword from 'src/components/ResetPassword';
import Spinner from 'src/components/Spinner';

const ResetPasswordPage = ({ match, checkResetPasswordToken: checkToken, changePassword: change }) => {
  const { params } = match;
  const { token: resetPasswordToken } = params;
  const [isValidToken, setIsValidToken] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    (async () => {
      try {
        await checkToken(resetPasswordToken);
        setIsValidToken(true);
      } catch {
        setIsValidToken(false);
      } finally {
        setIsLoading(false);
      }
    })();
  });

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        {isLoading && <Spinner />}
        {!isLoading && (
          <>
            <Logo />
            {isValidToken && (
              <>
                <ResetPassword changePassword={change} token={resetPasswordToken} />
              </>
            )}
            {!isValidToken && (
              <>
                <Header as="h2" color="red" textAlign="center">
                  Your token is invalid or expired.
                </Header>
                <Message>
                  <Message.Header>Please, use the link below to reset  token and try again</Message.Header>
                  <NavLink exact to="/forgotpassword">Reset token</NavLink>
                </Message>
              </>
            )}
          </>
        )}
      </Grid.Column>
    </Grid>
  );
};

ResetPasswordPage.propTypes = {
  checkResetPasswordToken: PropTypes.func.isRequired,
  changePassword: PropTypes.func.isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired
};

const actions = { checkResetPasswordToken, changePassword };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(ResetPasswordPage);
