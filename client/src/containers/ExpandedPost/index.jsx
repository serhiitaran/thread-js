import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment,
  getUsersWhoLikedPost,
  getUsersWhoDislikedPost,
  getUsersWhoLikedComment,
  getUsersWhoDislikedComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import UpdateComment from 'src/components/UpdateComment';
import DeleteItem from 'src/components/DeleteItem';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  editPost,
  removePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  updateComment: update,
  deleteComment: remove,
  likeComment: commentReactLike,
  dislikeComment: commentReactDislike,
  getUsersWhoLikedPost: getUsersLikedPost,
  getUsersWhoDislikedPost: getUsersDislikedPost,
  getUsersWhoLikedComment: getUsersLikedComment,
  getUsersWhoDislikedComment: getUsersDislikedComment,
  userId
}) => {
  const [editCommentContent, setEditCommentContent] = useState(undefined);
  const [removeCommentId, setRemoveCommentId] = useState(undefined);

  const editComment = comment => {
    setEditCommentContent(comment);
  };

  const removeComment = id => {
    setRemoveCommentId(id);
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              likePost={like}
              dislikePost={dislike}
              toggleExpandedPost={toggle}
              sharePost={sharePost}
              editPost={editPost}
              removePost={removePost}
              getUsersLikedPost={getUsersLikedPost}
              getUsersDislikedPost={getUsersDislikedPost}
              isOwnPost={userId === post.userId}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    userId={userId}
                    editComment={editComment}
                    removeComment={removeComment}
                    likeComment={commentReactLike}
                    dislikeComment={commentReactDislike}
                    getUsersLikedComment={getUsersLikedComment}
                    getUsersDislikedComment={getUsersDislikedComment}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
            {editCommentContent && (
              <UpdateComment
                comment={editCommentContent}
                updateComment={update}
                close={() => setEditCommentContent(undefined)}
              />
            )}
            {removeCommentId && (
              <DeleteItem
                title="comment"
                id={removeCommentId}
                deleteItem={remove}
                close={() => setRemoveCommentId(undefined)}
              />
            )}
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  removePost: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  getUsersWhoLikedPost: PropTypes.func.isRequired,
  getUsersWhoDislikedPost: PropTypes.func.isRequired,
  getUsersWhoLikedComment: PropTypes.func.isRequired,
  getUsersWhoDislikedComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment,
  getUsersWhoLikedPost,
  getUsersWhoDislikedPost,
  getUsersWhoLikedComment,
  getUsersWhoDislikedComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
