import React from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid } from 'semantic-ui-react';
import UserName from 'src/components/UserName';
import UserEmail from 'src/components/UserEmail';
import UserAvatar from 'src/components/UserAvatar';
import UserStatus from 'src/components/UserStatus';
import { updateUser } from './actions';

const Profile = ({ user, updateUser: editUser }) => (
  <Grid container textAlign="center" style={{ paddingTop: 30 }}>
    <Grid.Column>
      <UserAvatar user={user} updateAvatar={editUser} />
      <UserName user={user} updateName={editUser} />
      <UserEmail email={user.email} />
      <UserStatus user={user} updateStatus={editUser} />
    </Grid.Column>
  </Grid>
);

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateUser: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  updateUser
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
