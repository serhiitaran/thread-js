import * as authService from 'src/services/authService';
import * as userService from 'src/services/userService';
import { SET_USER } from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const updateUser = (id, data) => async (dispatch, getRootState) => {
  const user = await userService.updateUser(id, data);
  setUser(user)(dispatch, getRootState);
};

export const recoverPassword = request => async () => authService.recoverPassword(request);

export const checkResetPasswordToken = token => async () => (
  authService.checkResetPasswordToken(token));

export const changePassword = (token, request) => async () => authService.changePassword(token, request);
