import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { recoverPassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import ForgotPassword from 'src/components/ForgotPassword';

const ForgotPasswordPage = ({ recoverPassword: recover }) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <ForgotPassword recoverPassword={recover} />
      <Message>
        <NavLink exact to="/login">Back to login</NavLink>
      </Message>
      <Message>
        New to us?
        {' '}
        <NavLink exact to="/registration">Sign Up</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

ForgotPasswordPage.propTypes = {
  recoverPassword: PropTypes.func.isRequired
};

const actions = { recoverPassword };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(ForgotPasswordPage);

