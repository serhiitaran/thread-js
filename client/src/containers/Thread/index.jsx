/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import UpdatePost from 'src/components/UpdatePost';
import DeleteItem from 'src/components/DeleteItem';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  updatePost,
  deletePost,
  getUsersWhoLikedPost,
  getUsersWhoDislikedPost,
  sharePostByEmail
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  updatePost: update,
  deletePost: remove,
  getUsersWhoLikedPost: getUsersLikedPost,
  getUsersWhoDislikedPost: getUsersDislikedPost,
  sharePostByEmail: shareByEmail
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [editPostContent, setEditPostContent] = useState(undefined);
  const [deletePostId, setDeletePostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);
  const [showLikedByMePosts, setShowLikedByMePosts] = useState(false);

  const updatePostsFilter = (filter, from, filterType) => {
    postsFilter.userId = filter ? undefined : userId;
    postsFilter.from = from;
    postsFilter.filterType = filterType;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setHideOwnPosts(false);
    setShowLikedByMePosts(false);
    updatePostsFilter(showOwnPosts, 0, 'showOwnPosts');
  };

  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    setShowOwnPosts(false);
    setShowLikedByMePosts(false);
    updatePostsFilter(hideOwnPosts, 0, 'hideOwnPosts');
  };

  const toggleShowLikedByMePosts = () => {
    setShowLikedByMePosts(!showLikedByMePosts);
    setShowOwnPosts(false);
    setHideOwnPosts(false);
    updatePostsFilter(showLikedByMePosts, 0, 'showLikedByMePosts');
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const editPost = post => {
    setEditPostContent(post);
  };

  const removePost = id => {
    setDeletePostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Hide my posts"
          checked={hideOwnPosts}
          onChange={toggleHideOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only liked by me posts"
          checked={showLikedByMePosts}
          onChange={toggleShowLikedByMePosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            isOwnPost={userId === post.userId}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            editPost={editPost}
            removePost={removePost}
            key={post.id}
            getUsersLikedPost={getUsersLikedPost}
            getUsersDislikedPost={getUsersDislikedPost}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
          userId={userId}
          editPost={editPost}
          removePost={removePost}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          sharePostByEmail={shareByEmail}
          close={() => setSharedPostId(undefined)}
        />
      )}
      {editPostContent && (
        <UpdatePost
          post={editPostContent}
          updatePost={update}
          uploadImage={uploadImage}
          close={() => setEditPostContent(undefined)}
        />
      )}
      {deletePostId && (
        <DeleteItem
          title="post"
          id={deletePostId}
          deleteItem={remove}
          close={() => setDeletePostId(undefined)}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  getUsersWhoLikedPost: PropTypes.func.isRequired,
  getUsersWhoDislikedPost: PropTypes.func.isRequired,
  sharePostByEmail: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  updatePost,
  deletePost,
  getUsersWhoLikedPost,
  getUsersWhoDislikedPost,
  sharePostByEmail
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
