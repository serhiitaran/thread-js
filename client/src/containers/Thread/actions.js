import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  UPDATE_POST,
  DELETE_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const updatedPostAction = post => ({
  type: UPDATE_POST,
  post
});

const deletePostAction = id => ({
  type: DELETE_POST,
  id
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const applyUpdatePost = postId => async (dispatch, getRootState) => {
  const updatedPost = await postService.getPost(postId);
  dispatch(updatedPostAction(updatedPost));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction((updatedPost)));
  }
};

export const updatePost = (postId, request) => async (dispatch, getRootState) => {
  const updatedPost = await postService.updatePost(postId, request);
  dispatch(updatedPostAction(updatedPost));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction((updatedPost)));
  }
};

export const applyDeletePost = postId => (dispatch, getRootState) => {
  dispatch(deletePostAction(postId));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction((null)));
  }
};

export const deletePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.deletePost(postId);
  dispatch(deletePostAction(id));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === id) {
    dispatch(setExpandedPostAction((null)));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.likePost(postId);
  const { dislikeCount } = await postService.getPost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapLikes = post => ({
    ...post,
    dislikeCount,
    likeCount: Number(post.likeCount) + diff // diff is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.dislikePost(postId);
  const { likeCount } = await postService.getPost(postId);
  const diff = id ? 1 : -1;

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff,
    likeCount
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

const updatePostCommentAction = async (postId, dispatch, getRootState) => {
  const updatedPost = await postService.getPost(postId);
  dispatch(updatedPostAction(updatedPost));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const updateComment = (id, request) => async (dispatch, getRootState) => {
  const { postId } = await commentService.updateComment(id, request);
  updatePostCommentAction(postId, dispatch, getRootState);
};

export const deleteComment = id => async (dispatch, getRootState) => {
  const { postId } = await commentService.deleteComment(id);
  updatePostCommentAction(postId, dispatch, getRootState);
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  await commentService.likeComment(commentId);
  const { postId } = await commentService.getComment(commentId);
  updatePostCommentAction(postId, dispatch, getRootState);
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  await commentService.dislikeComment(commentId);
  const { postId } = await commentService.getComment(commentId);
  updatePostCommentAction(postId, dispatch, getRootState);
};

export const applyReactComment = commentId => async (dispatch, getRootState) => {
  const { postId } = await commentService.getComment(commentId);
  updatePostCommentAction(postId, dispatch, getRootState);
};

export const getUsersWhoLikedPost = postId => async (dispatch, getRootState) => {
  const usersWhoLikedPost = await postService.getUsersWhoLiked(postId);
  const mapUsersWhoLiked = post => ({
    ...post,
    usersWhoLikedPost
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapUsersWhoLiked(post)));

  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapUsersWhoLiked(expandedPost)));
  }
};

export const getUsersWhoDislikedPost = postId => async (dispatch, getRootState) => {
  const usersWhoDislikedPost = await postService.getUsersWhoDisliked(postId);
  const mapUsersWhoDisliked = post => ({
    ...post,
    usersWhoDislikedPost
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapUsersWhoDisliked(post)));

  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapUsersWhoDisliked(expandedPost)));
  }
};

export const getUsersWhoLikedComment = commentId => async (dispatch, getRootState) => {
  const usersWhoLikedComment = await commentService.getUsersWhoLiked(commentId);
  const { postId } = await commentService.getComment(commentId);
  const mapUsersWhoLikedComment = comment => ({
    ...comment,
    usersWhoLikedComment
  });
  const { posts: { expandedPost } } = getRootState();
  const comments = expandedPost.comments.map(comment => (
    (comment.id !== commentId ? comment : mapUsersWhoLikedComment(comment))));
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction({ ...expandedPost, comments }));
  }
};

export const getUsersWhoDislikedComment = commentId => async (dispatch, getRootState) => {
  const usersWhoDislikedComment = await commentService.getUsersWhoDisliked(commentId);
  const { postId } = await commentService.getComment(commentId);
  const mapUsersWhoDislikedComment = comment => ({
    ...comment,
    usersWhoDislikedComment
  });
  const { posts: { expandedPost } } = getRootState();
  const comments = expandedPost.comments.map(comment => (
    (comment.id !== commentId ? comment : mapUsersWhoDislikedComment(comment))));
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction({ ...expandedPost, comments }));
  }
};

export const sharePostByEmail = request => async () => postService.sharePostByEmail(request);
