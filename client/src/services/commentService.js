import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const updateComment = async (id, request) => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'PUT',
    request
  });
  return response.json();
};

export const deleteComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'DELETE'
  });
  return response.json();
};

export const likeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      isLike: true,
      commentId
    }
  });
  return response.json();
};

export const dislikeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      isLike: false,
      commentId
    }
  });
  return response.json();
};

export const getUsersWhoLiked = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'GET',
    query: {
      commentId,
      isLike: true
    }
  });
  return response.json();
};

export const getUsersWhoDisliked = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'GET',
    query: {
      commentId,
      isLike: false
    }
  });
  return response.json();
};
