import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Header, Segment, Form, Button } from 'semantic-ui-react';

import { checkMessages } from 'src/helpers/utils';

const UpdateComment = ({ comment: { id, body }, updateComment, close }) => {
  const [editBody, setEditBody] = useState(body);

  const handleUpdatePost = async () => {
    const isSameMessages = checkMessages(body, editBody);
    if (!editBody || isSameMessages) {
      return;
    }
    updateComment(id, { body: editBody });
    close();
  };

  return (
    <Modal open onClose={close}>
      <Header as="h3" dividing>
        Update Comment
      </Header>
      <Segment basic>
        <Form onSubmit={handleUpdatePost}>
          <Form.TextArea
            name="body"
            value={editBody}
            onChange={ev => setEditBody(ev.target.value)}
          />
          <Button color="blue" type="submit">Update Comment</Button>
          <Button basic onClick={close}>Cancel</Button>
        </Form>
      </Segment>
    </Modal>
  );
};

UpdateComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  close: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired
};

export default UpdateComment;
