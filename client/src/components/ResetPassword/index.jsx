import React, { useState } from 'react';
import { Form, Button, Segment, Header, Message } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

const ResetPassword = ({ changePassword, token }) => {
  const [password, setPassword] = useState('');
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [isPasswordChanged, setIsPasswordChanged] = useState(false);

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleChangePassword = async () => {
    setIsLoading(true);
    try {
      await changePassword(token, { password });
      setIsPasswordChanged(true);
    } catch (error) {
      NotificationManager.error(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      {!isPasswordChanged && (
        <>
          <Header as="h2" color="teal" textAlign="center">
            Enter your new password
          </Header>
          <Form name="resetPasswordForm" size="large" onSubmit={handleChangePassword}>
            <Segment>
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                type="password"
                error={!isPasswordValid}
                onChange={ev => passwordChanged(ev.target.value)}
                onBlur={() => setIsPasswordValid(Boolean(password))}
              />
              <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
                Change password
              </Button>
            </Segment>
          </Form>
        </>
      )}
      {isPasswordChanged && (
        <>
          <Header as="h2" color="teal" textAlign="center">
            Your password was successfully updated!
          </Header>
          <Message>
            <Message.Header>Please, use the link below to sign-in</Message.Header>
            <NavLink exact to="/login">Sign In</NavLink>
          </Message>
        </>
      )}
    </>
  );
};

ResetPassword.propTypes = {
  changePassword: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired
};

export default ResetPassword;
