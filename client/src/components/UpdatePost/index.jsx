import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Header, Segment, Form, Button, Icon, Image } from 'semantic-ui-react';

import { checkMessages, checkImages } from 'src/helpers/utils';
import styles from './styles.module.scss';

const UpdatePost = ({ post, updatePost, uploadImage, close }) => {
  const { id, body, image } = post;
  const oldImage = image ? { imageId: image.id, imageLink: image.link } : undefined;
  const defaultImgBtnTitle = image ? 'Change image' : 'Attach image';

  const [editBody, setEditBody] = useState(body);
  const [img, setImg] = useState(oldImage);
  const [imgBtnTitle, setImgBtnTitle] = useState(defaultImgBtnTitle);
  const [resetInput, setResetInput] = useState(Date.now());
  const [isUploading, setIsUploading] = useState(false);

  const handleUpdatePost = async () => {
    const isSameMessages = checkMessages(body, editBody);
    const isSameImages = checkImages(oldImage, img);
    if (!editBody || (isSameMessages && isSameImages)) {
      return;
    }
    updatePost(id, { body: editBody, imageId: img ? img.imageId : null });
    close();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImg({ imageId, imageLink });
    } finally {
      setIsUploading(false);
      setImgBtnTitle('Change image');
    }
  };

  const handleRemoveImage = () => {
    setImg(undefined);
    setResetInput(Date.now());
    setImgBtnTitle('Attach image');
  };

  return (
    <Modal open onClose={close}>
      <Header as="h3" dividing>
        Update Post
      </Header>
      <Segment basic>
        <Form onSubmit={handleUpdatePost}>
          <Form.TextArea
            name="body"
            value={editBody}
            onChange={ev => setEditBody(ev.target.value)}
          />
          {img?.imageLink && (
            <div className={styles.imageWrapper}>
              <Image className={styles.image} src={img?.imageLink} alt="post" />
            </div>
          )}
          <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
            <Icon name="image" />
            {imgBtnTitle}
            <input name="image" type="file" key={resetInput} onChange={handleUploadFile} hidden />
          </Button>
          {img && (
            <Button color="red" icon labelPosition="left" as="label" onClick={handleRemoveImage}>
              <Icon name="trash" />
              Remove image
            </Button>
          )}
          <Button color="blue" type="submit">Update Post</Button>
        </Form>
      </Segment>
    </Modal>
  );
};

UpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  close: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default UpdatePost;
