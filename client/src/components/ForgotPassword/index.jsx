import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment, Header } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';

const ForgotPassword = ({ recoverPassword }) => {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isEmailSent, setIsEmailSent] = useState(false);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleResetPassword = async () => {
    if (!isEmailValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await recoverPassword({ email });
      setIsEmailSent(true);
    } catch (error) {
      NotificationManager.error(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      {!isEmailSent && (
        <>
          <Header as="h2" color="teal" textAlign="center">
            Forgot your password?
          </Header>
          <p>Enter your email. We&apos;ll email instructions on how to reset your password.</p>
          <Form name="forgotPasswordForm" size="large" onSubmit={handleResetPassword}>
            <Segment>
              <Form.Input
                fluid
                icon="at"
                iconPosition="left"
                placeholder="Email"
                type="email"
                error={!isEmailValid}
                onChange={ev => emailChanged(ev.target.value)}
                onBlur={() => setIsEmailValid(validator.isEmail(email))}
              />
              <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
                Submit
              </Button>
            </Segment>
          </Form>
        </>
      )}

      {isEmailSent && (
        <>
          <Header as="h2" color="teal" textAlign="center">
            Password Reset Email Sent
          </Header>
          <p>
            Check your inbox for a password reset email.
            Click on the URL provided in the email and enter a new password.
          </p>
        </>
      )}
    </>
  );
};

ForgotPassword.propTypes = {
  recoverPassword: PropTypes.func.isRequired
};

export default ForgotPassword;
