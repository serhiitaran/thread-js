import React from 'react';
import { List } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import ReactionUsersItem from 'src/components/Reaction/ReactionUsersItem';
import styles from './styles.module.scss';

const ReactionUsersList = ({ users, notFoundTitle }) => (
  <List size="tiny" verticalAlign="middle">
    {!users.length ? (
      <p className={styles.reactionUsersListNotFound}>{notFoundTitle}</p>) : (
      users.map(user => <ReactionUsersItem user={user} key={user.id} />))}
  </List>
);

ReactionUsersList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  notFoundTitle: PropTypes.string.isRequired
};

export default ReactionUsersList;
