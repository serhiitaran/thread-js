import React from 'react';
import { List, Image } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { getUserImgLink } from 'src/helpers/imageHelper';

const ReactionUsersItem = ({ user }) => {
  const imgLink = getUserImgLink(user.image);
  return (
    <List.Item>
      <Image avatar src={imgLink} />
      <List.Content>
        <List.Header>{user.username}</List.Header>
      </List.Content>
    </List.Item>
  );
};

ReactionUsersItem.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ReactionUsersItem;
