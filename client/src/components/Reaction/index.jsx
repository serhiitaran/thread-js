import React, { useState } from 'react';
import { Popup, Icon, Dimmer, Loader } from 'semantic-ui-react';
import ReactionUsersList from 'src/components/Reaction/ReactionUsersList';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

const Reaction = ({ type, reactCount, users, getUsers }) => {
  const [isLoading, setIsLoading] = useState(false);
  const reactIcon = type === 'like' ? 'thumbs up' : 'thumbs down';
  const notFoundTitle = type === 'like' ? 'No one liked' : 'No one disliked';
  const handleGetUsers = async () => {
    setIsLoading(true);
    await getUsers();
    setIsLoading(false);
  };
  return (
    <Popup
      className={styles.reactionPopup}
      position="top center"
      hoverable
      trigger={(
        <span>
          <Icon name={reactIcon} />
          {reactCount}
        </span>
      )}
      onOpen={handleGetUsers}
      content={(
        <div className={styles.reactionPopupContainer}>
          {isLoading ? (
            <Dimmer active inverted>
              <Loader />
            </Dimmer>
          ) : <ReactionUsersList users={users} notFoundTitle={notFoundTitle} />}
        </div>
      )}
      popperModifiers={{
        preventOverflow: {
          boundariesElement: 'offsetParent'
        }
      }}
    />
  );
};

Reaction.propTypes = {
  type: PropTypes.string.isRequired,
  reactCount: PropTypes.number.isRequired,
  users: PropTypes.arrayOf(PropTypes.object),
  getUsers: PropTypes.func.isRequired
};

Reaction.defaultProps = {
  users: []
};

export default Reaction;
