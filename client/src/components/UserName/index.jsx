import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Header, Input, Button } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import { usernameValidation } from 'src/helpers/validationHelper';

import styles from './styles.module.scss';

const UserName = ({ user, updateName }) => {
  const { id, username: currentUsername } = user;
  const USERNAME_VALID_LENGTH = { min: 3, max: 50 };

  const [username, setUsername] = useState(currentUsername);
  const [isEditing, setIsEditing] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const [isUpdating, setIsUpdating] = useState(false);

  const handleEditingUsername = () => {
    setIsDisabled(false);
    setIsEditing(true);
  };

  const handleDiscardChanges = () => {
    setIsEditing(false);
    setUsername(currentUsername);
    setIsDisabled(true);
  };

  const handleUpdateUsername = async () => {
    try {
      usernameValidation(currentUsername, username, USERNAME_VALID_LENGTH);
      setIsUpdating(true);
      await updateName(id, { username });
      NotificationManager.info('Your username was successfully updated!');
      setIsEditing(false);
      setIsDisabled(true);
    } catch (error) {
      NotificationManager.error(error.message);
    } finally {
      setIsUpdating(false);
    }
  };

  return (
    <>
      <Header as="h3">Username</Header>
      <Input
        icon="user"
        iconPosition="left"
        placeholder="Username"
        type="text"
        disabled={isDisabled}
        value={username}
        onChange={ev => setUsername(ev.target.value)}
      />
      <br />
      <div className={styles.buttonsWrapper}>
        {!isEditing && (
          <Button size="medium" basic compact onClick={handleEditingUsername}>Edit username</Button>
        )}
        {isEditing && (
          <>
            <Button size="medium" basic compact onClick={handleDiscardChanges}>Cancel</Button>
            <Button
              size="medium"
              basic
              loading={isUpdating}
              compact
              onClick={handleUpdateUsername}
            >
              Save username
            </Button>
          </>
        )}
      </div>

    </>
  );
};

UserName.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateName: PropTypes.func.isRequired
};

export default UserName;
