import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Header, Input, Button, Icon } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import { statusValidation } from 'src/helpers/validationHelper';

import styles from './styles.module.scss';

const UserStatus = ({ user, updateStatus, headerStatus }) => {
  const STATUS_VALID_LENGTH = { min: 1, max: 50 };

  const [status, setStatus] = useState(user.status);
  const [isEditing, setIsEditing] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const [isUpdating, setIsUpdating] = useState(false);

  useEffect(
    () => {
      setStatus(user.status);
    },
    [user]
  );

  const handleEditingStatus = () => {
    setIsDisabled(false);
    setIsEditing(true);
  };

  const handleDiscardChanges = () => {
    setIsEditing(false);
    setStatus(user.status);
    setIsDisabled(true);
  };

  const handleDeleteStatus = async () => {
    try {
      await updateStatus(user.id, { status: '' });
      NotificationManager.info('Your status was successfully deleted!');
      setIsEditing(false);
    } catch (error) {
      NotificationManager.error(error.message);
    }
  };

  const handleUpdateStatus = async () => {
    try {
      statusValidation(user.status, status, STATUS_VALID_LENGTH);
      setIsUpdating(true);
      await updateStatus(user.id, { status });
      NotificationManager.info('Your status was successfully updated!');
      setIsEditing(false);
      setIsDisabled(true);
    } catch (error) {
      NotificationManager.error(error.message);
    } finally {
      setIsUpdating(false);
    }
  };

  return (
    <>
      {!headerStatus && <Header as="h3">Status</Header>}
      <Input
        icon="circle"
        iconPosition="left"
        placeholder="Status"
        type="text"
        disabled={isDisabled}
        value={status}
        onChange={ev => setStatus(ev.target.value)}
        className={headerStatus && styles.onlyEdit}
        size={headerStatus && 'mini'}
      />
      {headerStatus && (
        <>
          {!isEditing && (
            <Icon name="pencil" size="small" onClick={handleEditingStatus} />)}
          {isEditing && (
            <>
              <Button
                size="tiny"
                basic
                compact
                onClick={handleUpdateStatus}
              >
                Save status
              </Button>
              {user.status && <Button size="tiny" basic compact onClick={handleDeleteStatus}>Delete status</Button>}
              <Button size="tiny" basic compact onClick={handleDiscardChanges}>Cancel</Button>
            </>
          )}
        </>
      )}
      {!headerStatus && (
        <>
          <br />
          <div className={styles.buttonsWrapper}>
            {!isEditing && (
              <Button size="medium" basic compact onClick={handleEditingStatus}>
                {user.status ? 'Edit status' : 'Set status'}
              </Button>
            )}
            {user.status && <Button size="medium" basic compact onClick={handleDeleteStatus}>Delete status</Button>}
            {isEditing && (
              <>
                <Button size="medium" basic compact onClick={handleDiscardChanges}>Cancel</Button>
                <Button
                  size="medium"
                  basic
                  loading={isUpdating}
                  compact
                  onClick={handleUpdateStatus}
                >
                  Save status
                </Button>
              </>
            )}
          </div>
        </>
      )}

    </>
  );
};

UserStatus.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateStatus: PropTypes.func.isRequired,
  headerStatus: PropTypes.bool
};

UserStatus.defaultProps = {
  headerStatus: false
};

export default UserStatus;
