import React, { useState, useCallback } from 'react';
import { Header, Modal, Button, Image } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import ReactCrop from 'react-image-crop';
import PropTypes from 'prop-types';
import { getUserImgLink, getCroppedImg } from 'src/helpers/imageHelper';
import * as imageService from 'src/services/imageService';

import 'react-image-crop/dist/ReactCrop.css';
import styles from './styles.module.scss';

const UserAvatar = ({ user, updateAvatar }) => {
  const AVATAR_WIDTH = { min: 100, max: 200 };
  const { id: userId, image: currentAvatar } = user;

  const [loadedImage, setLoadedImage] = useState(null);
  const [imageSrc, setImageSrc] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [crop, setCrop] = useState({ aspect: 1 / 1, unit: 'px', width: AVATAR_WIDTH.min });
  const [isUploading, setIsUploading] = useState(false);
  const [resetInput, setResetInput] = useState(Date.now());

  const handleUploadImage = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setImageSrc(reader.result));
      reader.readAsDataURL(e.target.files[0]);
      setShowModal(true);
    }
  };

  const handleLoadedImage = useCallback(image => {
    setLoadedImage(image);
  }, []);

  const handleUpdateUserAvatar = async () => {
    try {
      setIsUploading(true);
      const croppedImageBlob = await getCroppedImg(loadedImage, crop);
      const croppedImage = new File([croppedImageBlob], 'image');
      const { id: imageId } = await imageService.uploadImage(croppedImage);
      await updateAvatar(userId, { imageId });
      setResetInput(Date.now());
      setShowModal(false);
      NotificationManager.info('Your avatar was successfully updated!');
    } catch (error) {
      NotificationManager.error(error.message);
    } finally {
      setIsUploading(false);
    }
  };

  const handleDeleteUserAvatar = async () => {
    try {
      await updateAvatar(userId, { imageId: null });
      NotificationManager.info('Your avatar was successfully deleted!');
    } catch (error) {
      NotificationManager.info(error);
    }
  };

  const handleDiscardChanges = () => {
    setResetInput(Date.now());
    setShowModal(false);
  };

  return (
    <>
      <Header as="h3">Avatar</Header>
      <>
        <Image className={styles.avatarPreview} centered circular src={getUserImgLink(currentAvatar)} />
        <div className={styles.buttonsWrapper}>
          <Button size="medium" basic compact as="label">
            Change avatar
            <input key={resetInput} name="image" accept="image/*" type="file" onChange={handleUploadImage} hidden />
          </Button>
          {currentAvatar && (
            <Button size="medium" basic compact onClick={handleDeleteUserAvatar}>Delete avatar</Button>)}
        </div>
      </>
      <Modal open={showModal} onClose={handleDiscardChanges} size="tiny">
        <Header as="h3" dividing>
          Edit avatar
        </Header>
        <Modal.Content>
          <ReactCrop
            src={imageSrc}
            crop={crop}
            onChange={setCrop}
            minWidth={AVATAR_WIDTH.min}
            maxWidth={AVATAR_WIDTH.max}
            onImageLoaded={handleLoadedImage}
          />
        </Modal.Content>
        <Modal.Actions>
          <Button color="red" onClick={handleDiscardChanges}>Cancel</Button>
          <Button color="blue" onClick={handleUpdateUserAvatar} loading={isUploading}>Apply</Button>
        </Modal.Actions>
      </Modal>
    </>
  );
};

UserAvatar.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateAvatar: PropTypes.func.isRequired
};

export default UserAvatar;
