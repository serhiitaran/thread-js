import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({ user, applyPost, applyUpdatePost, applyDeletePost, applyReactComment }) => {
  const { REACT_APP_SOCKET_SERVER: address } = process.env;
  const [socket] = useState(io(address));

  useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socket.emit('createRoom', id);
    socket.on('like', () => {
      NotificationManager.info('Your post was liked!');
    });
    socket.on('dislike', () => {
      NotificationManager.info('Your post was disliked!');
    });
    socket.on('like_сomment', () => {
      NotificationManager.info('Your comment was liked!');
    });
    socket.on('dislike_comment', () => {
      NotificationManager.info('Your comment was disliked!');
    });
    socket.on('new_post', post => {
      if (post.userId !== id) {
        applyPost(post.id);
      }
    });
    socket.on('uptated_post', post => {
      if (post.userId !== id) {
        applyUpdatePost(post.id);
      }
    });
    socket.on('deleted_post', post => {
      if (post.userId !== id) {
        applyDeletePost(post.id);
      }
    });
    socket.on('react_post', react => {
      if (react.userId !== id) {
        applyUpdatePost(react.postId);
      }
    });
    socket.on('new_comment', comment => {
      if (comment.userId !== id) {
        applyUpdatePost(comment.postId);
      }
    });
    socket.on('updated_comment', comment => {
      if (comment.userId !== id) {
        applyUpdatePost(comment.postId);
      }
    });
    socket.on('deleted_comment', comment => {
      if (comment.userId !== id) {
        applyUpdatePost(comment.postId);
      }
    });
    socket.on('react_comment', react => {
      if (react.userId !== id) {
        applyReactComment(react.commentId);
      }
    });
    return () => {
      socket.close();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired,
  applyUpdatePost: PropTypes.func.isRequired,
  applyDeletePost: PropTypes.func.isRequired,
  applyReactComment: PropTypes.func.isRequired
};

export default Notifications;
