import React from 'react';
import PropTypes from 'prop-types';
import { Header, Input } from 'semantic-ui-react';

const UserEmail = ({ email }) => (
  <>
    <Header as="h3">Email</Header>
    <Input
      icon="at"
      iconPosition="left"
      placeholder="Email"
      type="email"
      disabled
      value={email}
    />
  </>
);

UserEmail.propTypes = {
  email: PropTypes.string.isRequired
};

export default UserEmail;
