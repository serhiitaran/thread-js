import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import Reaction from 'src/components/Reaction';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({
  post,
  isOwnPost,
  likePost,
  dislikePost,
  toggleExpandedPost,
  sharePost,
  editPost,
  removePost,
  getUsersLikedPost,
  getUsersDislikedPost
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    updatedAt,
    usersWhoLikedPost,
    usersWhoDislikedPost
  } = post;
  const date = moment(createdAt).fromNow();
  const updatedDate = moment(updatedAt).fromNow();
  const isUpdated = updatedAt !== createdAt;
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          {isUpdated && (
            <>
              <br />
              <span className="date">
                edited
                {' - '}
                {updatedDate}
              </span>
            </>
          )}
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
          <Reaction
            reactCount={Number(likeCount)}
            type="like"
            users={usersWhoLikedPost}
            getUsers={() => getUsersLikedPost(id)}
          />
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Reaction
            reactCount={Number(dislikeCount)}
            type="dislike"
            users={usersWhoDislikedPost}
            getUsers={() => getUsersDislikedPost(id)}
          />
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {isOwnPost
          && (
            <>
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => editPost(post)}>
                <Icon name="edit" />
              </Label>
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => removePost(id)}>
                <Icon name="trash" />
              </Label>
            </>
          )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  isOwnPost: PropTypes.bool.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  removePost: PropTypes.func.isRequired,
  getUsersLikedPost: PropTypes.func.isRequired,
  getUsersDislikedPost: PropTypes.func.isRequired
};

export default Post;
