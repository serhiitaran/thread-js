import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Header, Segment, Button, Icon } from 'semantic-ui-react';

const DeleteItem = ({ title, id, deleteItem, close }) => {
  const handleDeleteItem = async () => {
    deleteItem(id);
    close();
  };

  return (
    <Modal open onClose={close}>
      <Header as="h3" dividing>
        {`Delete ${title}`}
      </Header>
      <Segment basic>
        <p>{`Are you sure you want to delete this ${title} ?`}</p>
        <Button color="red" icon labelPosition="left" as="label" onClick={handleDeleteItem}>
          <Icon name="trash" />
          Delete
        </Button>
        <Button basic onClick={() => close()}>
          Cancel
        </Button>
      </Segment>
    </Modal>
  );
};

DeleteItem.propTypes = {
  title: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  deleteItem: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default DeleteItem;
