import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import Reaction from 'src/components/Reaction';

import styles from './styles.module.scss';

const Comment = ({
  comment: {
    id,
    body,
    createdAt,
    updatedAt,
    user,
    likeCount,
    dislikeCount,
    usersWhoLikedComment,
    usersWhoDislikedComment
  },
  userId,
  editComment,
  removeComment,
  likeComment,
  dislikeComment,
  getUsersLikedComment,
  getUsersDislikedComment
}) => {
  const isUpdated = createdAt !== updatedAt;
  const isOwnComment = userId === user.id;
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
          {isUpdated && ' (edited)'}
        </CommentUI.Metadata>
        <CommentUI.Metadata className={styles.commentUserStatus}>
          {user.status}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Actions>
          <CommentUI.Action onClick={() => likeComment(id)}>
            <Reaction
              reactCount={Number(likeCount)}
              type="like"
              users={usersWhoLikedComment}
              getUsers={() => getUsersLikedComment(id)}
            />
          </CommentUI.Action>
          <CommentUI.Action onClick={() => dislikeComment(id)}>
            <Reaction
              reactCount={Number(dislikeCount)}
              type="dislike"
              users={usersWhoDislikedComment}
              getUsers={() => getUsersDislikedComment(id)}
            />
          </CommentUI.Action>
          {isOwnComment && (
            <>
              <CommentUI.Action onClick={() => editComment({ id, body })}>
                <Icon name="edit" />
                Edit
              </CommentUI.Action>
              <CommentUI.Action onClick={() => removeComment(id)}>
                <Icon name="trash" />
                Delete
              </CommentUI.Action>
            </>
          )}
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  editComment: PropTypes.func.isRequired,
  removeComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  getUsersLikedComment: PropTypes.func.isRequired,
  getUsersDislikedComment: PropTypes.func.isRequired
};

export default Comment;
