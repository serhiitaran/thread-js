import { checkMessages, checkLength, capitalizeFirstLetter } from './utils';

const baseStringValidation = (itemName, currentItem, updatedItem, validLength) => {
  const isValidLength = checkLength(updatedItem, validLength);
  if (!isValidLength) {
    throw new Error(`
      ${capitalizeFirstLetter(itemName)} must be at least ${validLength.min} - ${validLength.max} characters long
    `);
  }
  const isSameNames = checkMessages(currentItem, updatedItem);
  if (isSameNames) {
    throw new Error(`It’s the same ${itemName}`);
  }
};

export const usernameValidation = (currentUsername, updatedUsername, validLength) => (
  baseStringValidation('username', currentUsername, updatedUsername, validLength)
);

export const statusValidation = (currentStatus, updatedStatus, validLength) => (
  baseStringValidation('status', currentStatus, updatedStatus, validLength)
);
