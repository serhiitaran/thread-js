export const checkImages = (oldImg, newImg) => {
  if (!oldImg && !newImg) {
    return false;
  }
  if (oldImg?.imageId === newImg?.imageId) {
    return true;
  }
  return false;
};

export const checkMessages = (oldMessage, newMessage) => {
  if (!oldMessage) {
    return false;
  }
  return oldMessage.trim() === newMessage.trim();
};

export const checkLength = (string, validLength) => (
  string.trim().length >= validLength.min && string.trim().length <= validLength.max);

export const capitalizeFirstLetter = string => {
  if (!string) {
    return string;
  }

  return string[0].toUpperCase() + string.slice(1);
};
